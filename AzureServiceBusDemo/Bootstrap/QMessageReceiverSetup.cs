﻿using AzureServiceBusDemo.Core.Framework;
using AzureServiceBusDemo.ServiceBus;
using Microsoft.AspNetCore.Builder;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Bootstrap
{
    public static class QMessageReceiverSetup
    {
        public static void UseQMessageReceiver(this IApplicationBuilder app)
        {
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler().GetAwaiter().GetResult();

            scheduler.Start();

            var queueReceiverJob = JobBuilder.Create<QueueReceiverJob>()
                                    .WithIdentity("queueReceiverJob", "queueing")
                                    .Build();

            var jobTrigger = TriggerBuilder.Create()
                              .WithIdentity("queueReceiverTrigger", "queueing")
                              .StartAt(DateTimeOffset.UtcNow.AddMinutes(5))
                              .WithSimpleSchedule(x => x
                                  .WithIntervalInMinutes(2)
                                  .RepeatForever())
                              .Build();

            scheduler.ScheduleJob(queueReceiverJob, jobTrigger);
        }
    }

    public class QueueReceiverJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
          {
            await IocContainerProvider.RunInLifetimeScopeAsync(async scope =>
            {
                var messageReceiver = new QMessageReceiver(scope);

                await messageReceiver.ReceiveMessages();
            });
        }
    }
}
