﻿using Autofac;
using AzureServiceBusDemo.AppService.Service;
using AzureServiceBusDemo.Core.IRepository;
using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Data.Sql.Repository;
using AzureServiceBusDemo.ServiceBus;

namespace AzureServiceBusDemo.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserEntityRepository>()
                .As<IUserEntityRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserEntityService>()
                .As<IUserEntityService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserContext>()
                .As<IUserContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MessageBus>()
              .As<IMessageBus>()
              .SingleInstance();

            builder.RegisterType<AppSettings>()
                .As<IAppSettings>()
                .SingleInstance();

            // register ids4 repositories 
        //    builder.RegisterAssemblyTypes(typeof(UserEntityRepository).Assembly)
        //        .Where(t => t.GetInterfaces().Any(i => i.Name.EndsWith("Repository")))
        }
    }
}
