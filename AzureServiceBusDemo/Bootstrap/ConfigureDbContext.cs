﻿using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Data.Sql.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<AzureServiceBusDbContext>(conf =>
            {
                conf.UseSqlServer(new AppSettings(Ioc.Configuration).DbConnectionString);
            });
        }
    }
}
