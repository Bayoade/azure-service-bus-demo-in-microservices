﻿using Microsoft.Extensions.Configuration;

namespace AzureServiceBusDemo.Bootstrap
{
    public static class Ioc
    {
        public static IConfiguration Configuration { get; set; }
    }
}
