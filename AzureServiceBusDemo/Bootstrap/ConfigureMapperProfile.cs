﻿using AutoMapper;
using AzureServiceBusDemo.Data.Sql;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Bootstrap
{
    public static class ConfigureMapperProfile
    {
        public static void AddMapperProfile(this IServiceCollection services)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<ServiceMapperProfile>();
            });
        }
    }
}
