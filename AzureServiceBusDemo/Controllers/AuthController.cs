﻿using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserEntityService _userEntityService;
        public AuthController(IUserEntityService userEntityService)
        {
            _userEntityService = userEntityService;
        }

        /// <summary>
        /// Create a user entity in the system
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create()
        {
            var userEntity = new UserEntity
            {
                FirstName = "Tolu",
                LastName = "Michael",
                Email = "tolumike@yahoo.com",
                PhoneNumber = "08037282211"
            };
            var newId = await _userEntityService.CreateUserEntityAsync(userEntity);
            return Ok(newId);
        }

    }   
}