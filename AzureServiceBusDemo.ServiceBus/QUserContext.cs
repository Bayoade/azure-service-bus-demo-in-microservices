﻿using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using System;

namespace AzureServiceBusDemo.ServiceBus
{
    public class QUserContext
    { 
        private readonly IUserEntityService _userEntityService;
        //private UserEntity _user;

        public QUserContext(
            Guid userId,
            IUserEntityService userEntityService)
        {
            UserId = userId;
            _userEntityService = userEntityService;
        }

        public Guid UserId { get; private set; }

        public string Email => GetUser().Email;

        public string FirstName => GetUser().FirstName;

        public string LastName => GetUser().LastName;

        public string Name => GetUser().FullName;
    
        private UserEntity GetUser()
        {
            return  _userEntityService.GetUserEntityAsync(UserId).GetAwaiter().GetResult();
        }
    }
}
