﻿using AzureServiceBusDemo.Core.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.ServiceBus
{
    public static class QueueFactory
    {
        public static async Task<CloudQueue> GetQueueAsync(IAppSettings appSettings)
        {
            var storageAccount = CloudStorageAccount.Parse(appSettings.QueueConnectionString);
            var queueClient = storageAccount.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference(appSettings.QueueName);

            await queue.CreateIfNotExistsAsync();

            return queue;
        }
    }
}
