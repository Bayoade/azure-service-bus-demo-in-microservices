﻿using System;

namespace AzureServiceBusDemo.ServiceBus
{
    public class QMessage
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string MessageTypeName { get; set; }
        public string MessageAssemblyName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
