﻿using Autofac;
using AzureServiceBusDemo.Core.Framework;
using AzureServiceBusDemo.Core.Helpers;
using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Core.ServiceBus;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.ServiceBus
{
    public class QMessageReceiver
    {
        private const int MessageReadCount = 5;
        private CloudQueue _queue;
        private readonly ILifetimeScope _lifetimeScope;

        public QMessageReceiver(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public async Task ReceiveMessages()
        {
            var queue = await GetQueue();
            var messages = await queue.GetMessagesAsync(MessageReadCount);

            foreach (var message in messages)
            {
                await ProcessMessage(message);
            }
        }

        private async Task ProcessMessage(CloudQueueMessage qMessage)
        {
            var messageString = qMessage.AsString;
            var message = messageString.FromJson<QMessage>();
            var type = Type.GetType($"{message.MessageTypeName}, {message.MessageAssemblyName}");
            var innerMessage = message.Message.FromJson(type) as IMessage;

            await HandleEvent(innerMessage);

            if (!qMessage.IsNull())
            {
                DequeueMessage(qMessage);
            }
        }

        private async Task HandleEvent<TMessage>(TMessage message)
            where TMessage : IMessage
        {
            var genericType = typeof(IHandler<>);
            var handlerType = genericType.MakeGenericType(message.GetType());

            await IocContainerProvider.RunInLifetimeScopeAsync(scope =>
            {
                var handler = scope.Resolve(handlerType);

                var result = handlerType.InvokeMember(nameof(IHandler<IMessage>.HandleAsync),
                                BindingFlags.InvokeMethod, null, handler, new object[] { message });

                return result as Task;
            },
            builder =>
            {
                builder.Register(x => CreateUserContext(x, message))
                .As<IUserContext>()
                .InstancePerLifetimeScope();
            });
        }

        private async void DequeueMessage(CloudQueueMessage message)
        {
            var queue = await GetQueue();

            await queue.DeleteMessageAsync(message);
        }

        private async Task<CloudQueue> GetQueue()
        {
            if (_queue.IsNull())
            {
                var appSettings = _lifetimeScope.Resolve<IAppSettings>();

                _queue = await QueueFactory.GetQueueAsync(appSettings);
            }

            return _queue;
        }

        private static QUserContext CreateUserContext(IComponentContext componentContext, IMessage message)
        {
            return new QUserContext(
                    message.PerformedById,
                    componentContext.Resolve<IUserEntityService>());
        }
    }
}
