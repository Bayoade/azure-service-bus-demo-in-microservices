﻿using AzureServiceBusDemo.Core.ServiceBus;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.ServiceBus
{
    public interface IMessageBus
    {
        Task PublishAsync(IMessage message);
    }
}
