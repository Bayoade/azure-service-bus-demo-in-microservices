﻿using AzureServiceBusDemo.Core.Helpers;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Core.ServiceBus;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.ServiceBus
{
    public class MessageBus : IMessageBus
    {
        private CloudQueue _queue;
        private readonly IAppSettings _appSettings;

        public MessageBus(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public async Task PublishAsync(IMessage message)
        {
            var messageType = message.GetType();
            var qmessage = new QMessage
            {
                Created = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                Message = message.ToJson(),
                MessageAssemblyName = messageType.Assembly.FullName,
                MessageTypeName = messageType.FullName,
                Modified = DateTime.UtcNow
            };

            var cloudQueueMessage = new CloudQueueMessage(qmessage.ToJson());

            var queue = await GetQueue();

            await queue.AddMessageAsync(cloudQueueMessage);
        }

        private async Task<CloudQueue> GetQueue()
        {
            if (_queue.IsNull())
            {
                _queue = await QueueFactory.GetQueueAsync(_appSettings);
            }

            return _queue;
        }
    }
}
