﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureServiceBusDemo.Data.Sql.Models
{
    internal class User
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public bool IsDeleted { get; set; }
    }
}
