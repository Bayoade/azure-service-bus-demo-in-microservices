﻿using AutoMapper;
using AzureServiceBusDemo.Core.IRepository;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Data.Sql.Context;
using AzureServiceBusDemo.Data.Sql.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Data.Sql.Repository
{
    public class UserEntityRepository : IUserEntityRepository
    {
        private readonly AzureServiceBusDbContext _dbContext;

        public UserEntityRepository(AzureServiceBusDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task CreateUserEntityAsync(UserEntity userEntity)
        {
            var user = Mapper.Map<User>(userEntity);
            user.Created = DateTime.UtcNow;
            _dbContext.Users.Add(user);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteUserEntityAsync(Guid id)
        {
            var user = new User { Id = id };

            _dbContext.Users.Attach(user);

            _dbContext.Users.Remove(user);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<UserEntity>> GetAllUserEntityAsync()
        {
            var users = await _dbContext.Users.Where(x => !x.IsDeleted).ToListAsync();

            return Mapper.Map<List<UserEntity>>(users);
        }

        public async Task<UserEntity> GetUserEntityAsync(Guid id)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);

            return Mapper.Map<UserEntity>(user);
        }

        public async Task RemoveUserEntityAsync(Guid id)
        {
            var dbUser = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);

            dbUser.IsDeleted = true;
            _dbContext.Users.Update(dbUser);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUserEntityAsync(UserEntity userEntity)
        {
            var dbUser = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userEntity.Id && !x.IsDeleted);
            Mapper.Map(userEntity, dbUser);
            dbUser.Modified = DateTime.UtcNow;

            _dbContext.Users.Update(dbUser);

            await _dbContext.SaveChangesAsync();
        }
    }
}
