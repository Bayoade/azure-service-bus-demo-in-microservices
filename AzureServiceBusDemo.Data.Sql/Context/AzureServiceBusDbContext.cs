﻿using AzureServiceBusDemo.Data.Sql.Models;
using Microsoft.EntityFrameworkCore;

namespace AzureServiceBusDemo.Data.Sql.Context
{
    public class AzureServiceBusDbContext : DbContext
    {
        public AzureServiceBusDbContext(DbContextOptions<AzureServiceBusDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userModelBuilder = modelBuilder.Entity<User>().ToTable("Users");
            userModelBuilder.HasKey(x => x.Id);
        }
        internal DbSet<User> Users { get; set; }
    }
}
