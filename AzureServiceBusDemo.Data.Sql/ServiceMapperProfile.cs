﻿using AutoMapper;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Data.Sql.Models;

namespace AzureServiceBusDemo.Data.Sql
{
    public class ServiceMapperProfile : Profile
    {
        public ServiceMapperProfile()
        {
            CreateMap<User, UserEntity>()
                .ReverseMap()
                .ForMember(x => x.Created, y => y.Ignore())
                .ForMember(x => x.Modified, y => y.Ignore())
                .ForMember(x => x.IsDeleted, y => y.Ignore());
        }
    }
}
