﻿using AzureServiceBusDemo.Core.IRepository;
using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using AzureServiceBusDemo.Core.ServiceBus.Events;
using AzureServiceBusDemo.ServiceBus;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.AppService.Service
{
    public class UserEntityService : IUserEntityService
    {
        private readonly IUserEntityRepository _userEntityRepository;
        private readonly IMessageBus _messageBus;
        private readonly IUserContext _userContext;
        public UserEntityService(IUserEntityRepository userEntityRepository,
            IMessageBus messageBus,
            IUserContext userContext)
        {
            _userEntityRepository = userEntityRepository;
            _messageBus = messageBus;
            _userContext = userContext;
        }
        public async Task<Guid> CreateUserEntityAsync(UserEntity userEntity)
        {
            userEntity.Id = Guid.NewGuid();

            await _userEntityRepository.CreateUserEntityAsync(userEntity);

            await _messageBus.PublishAsync(new UserCreatedEvent(userEntity.Id, _userContext.UserId));

            return userEntity.Id;
        }

        public async Task DeleteUserEntityAsync(Guid id)
        {
            await _userEntityRepository.DeleteUserEntityAsync(id);

            await _messageBus.PublishAsync(new UserDisabledEvent(id, _userContext.UserId));
        }

        public Task<IList<UserEntity>> GetAllUserEntityAsync()
        {
            return _userEntityRepository.GetAllUserEntityAsync();
        }

        public Task<UserEntity> GetUserEntityAsync(Guid id)
        {
            return _userEntityRepository.GetUserEntityAsync(id);
        }

        public async Task RemoveUserEntityAsync(Guid id)
        {
            await _userEntityRepository.RemoveUserEntityAsync(id);

            await _messageBus.PublishAsync(new UserDisabledEvent(id, _userContext.UserId));
        }

        public async Task UpdateUserEntityAsync(UserEntity userEntity)
        {
            await _userEntityRepository.UpdateUserEntityAsync(userEntity);

            await _messageBus.PublishAsync(new UserDisabledEvent(userEntity.Id, _userContext.UserId));
        }
    }
}

