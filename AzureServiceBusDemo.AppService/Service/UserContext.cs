﻿using AzureServiceBusDemo.Core.IRepository;
using AzureServiceBusDemo.Core.IService;
using AzureServiceBusDemo.Core.Models;
using System;

namespace AzureServiceBusDemo.AppService.Service
{
    public class UserContext : IUserContext
    {
        private readonly IUserEntityRepository _userEntityRepository;
        public UserContext(IUserEntityRepository userEntityRepository)
        {
            _userEntityRepository = userEntityRepository;
        }

        public Guid UserId => Guid.NewGuid();
        public string UserName => "yommy";
        public string Email => "yommy@example.com";
        public string FirstName => "yommy";
        public string LastName => "Farinde";

        private UserEntity GetUser(Guid id)
        {
            return _userEntityRepository.GetUserEntityAsync(id).GetAwaiter().GetResult();
        }
    }
}
