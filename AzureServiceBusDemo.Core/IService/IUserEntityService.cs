﻿using AzureServiceBusDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Core.IService
{
    public interface IUserEntityService
    {
        Task<UserEntity> GetUserEntityAsync(Guid id);

        Task<IList<UserEntity>> GetAllUserEntityAsync();

        Task<Guid> CreateUserEntityAsync(UserEntity userEntity);

        Task DeleteUserEntityAsync(Guid id);

        Task RemoveUserEntityAsync(Guid id);

        Task UpdateUserEntityAsync(UserEntity userEntity);
    }
}
