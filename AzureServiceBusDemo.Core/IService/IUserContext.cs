﻿using System;

namespace AzureServiceBusDemo.Core.IService
{
    public interface IUserContext
    {
        Guid UserId { get; }

        string UserName { get;}
    
        string Email { get; }

        string FirstName { get; }

        string LastName { get; }
    }
}
