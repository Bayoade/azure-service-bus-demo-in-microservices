﻿using Microsoft.Extensions.Configuration;

namespace AzureServiceBusDemo.Core.Models
{
    public class AppSettings : IAppSettings
    {
        public AppSettings(IConfiguration configuration)
        {
            DbConnectionString = configuration.GetConnectionString("AzureServiceDbConnectionString");
            QueueConnectionString = configuration["QueueSettings:StorageConnectionString"];
            QueueName = configuration["QueueSettings:QueueName"];
        }

        public string DbConnectionString { get; }

        public string QueueConnectionString { get; }

        public string QueueName { get; }
    }

    public interface IAppSettings
    {
        string DbConnectionString { get; }

        string QueueConnectionString { get; }

        string QueueName { get; }
    }
}
