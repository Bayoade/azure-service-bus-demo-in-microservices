﻿using System;

namespace AzureServiceBusDemo.Core.ServiceBus
{
    public interface IMessage
    {
        Guid Id { get; }

        Guid PerformedById { get; }

        DateTime CreatedDate { get; }
    }
}
