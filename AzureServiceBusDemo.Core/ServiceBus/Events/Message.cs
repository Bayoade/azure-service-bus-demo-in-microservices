﻿using System;

namespace AzureServiceBusDemo.Core.ServiceBus.Events
{
    public class Message : IMessage
    {
        public Message(Guid performedById)
        {
            Id = Guid.NewGuid();
            PerformedById = performedById;
            CreatedDate = DateTime.UtcNow;
        }

        public Guid Id { get; private set; }

        public Guid PerformedById { get; private set; }

        public DateTime CreatedDate { get; private set; }
    }
}
