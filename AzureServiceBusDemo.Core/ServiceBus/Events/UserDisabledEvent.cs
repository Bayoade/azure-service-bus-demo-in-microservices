﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureServiceBusDemo.Core.ServiceBus.Events
{
    public class UserDisabledEvent : Message
    {
        public UserDisabledEvent(Guid userId, Guid performedById)
            : base(performedById)
        {
            UserId = userId;
        }

        public Guid UserId { get; private set; }
    }
}
