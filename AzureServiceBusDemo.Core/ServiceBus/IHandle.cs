﻿using System.Threading.Tasks;

namespace AzureServiceBusDemo.Core.ServiceBus
{
    public interface IHandler<TMessage>
        where TMessage : IMessage
    {
        Task HandleAsync(TMessage message);
    }
}
