﻿using AzureServiceBusDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusDemo.Core.IRepository
{
    public interface IUserEntityRepository
    {
        Task<UserEntity> GetUserEntityAsync(Guid id);

        Task<IList<UserEntity>> GetAllUserEntityAsync();

        Task CreateUserEntityAsync(UserEntity userEntity);

        Task DeleteUserEntityAsync(Guid id);

        Task RemoveUserEntityAsync(Guid id);

        Task UpdateUserEntityAsync(UserEntity userEntity);
    }
}
